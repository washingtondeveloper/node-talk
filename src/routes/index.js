const authenticate = require("../middlewares/authenticate");

module.exports = (app) => {
  const { HomeController, ContactsController } = app.controllers;

  /**
   * Login
   */
  app.get("/", HomeController.index);
  app.post("/login", HomeController.login);
  app.get("/logout", HomeController.logout);

  /**
   * Contacts
   */
  app.get("/contatos", authenticate, ContactsController.index);

  app.get("/contatos/:id", authenticate, ContactsController.show);
  app.post("/contatos", authenticate, ContactsController.create);
  app.get("/contatos/:id/editar", authenticate, ContactsController.edit);
  app.put("/contatos/:id", authenticate, ContactsController.update);
  app.delete("/contatos", authenticate, ContactsController.destroy);
};
