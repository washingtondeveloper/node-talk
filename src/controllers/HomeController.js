class HomeController {
  constructor(app) {
    this.app = app;
  }

  index = (req, res) => {
    return res.render("login/index");
  };

  login = (req, res) => {
    const { user } = req.body;
    const { name, email } = user;

    if (email && name) {
      user.contacts = [];
      req.session.user = user;
      return res.redirect("/contatos");
    } else {
      return res.redirect("/");
    }
  };

  logout = (req, res) => {
    req.session.destroy();
    res.redirect("/");
  };
}

module.exports = (app) => {
  return new HomeController(app);
};
