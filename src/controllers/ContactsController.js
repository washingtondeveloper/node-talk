class ContactsContoller {
  constructor(app) {
    this.app = app;
  }

  index = (req, res) => {
    const { user } = req.session;
    const { contacts } = user;

    res.render("contacts/index", { user, contacts });
  };

  show = (req, res) => {
    const { id } = req.params;
    const { user } = req.session;

    const contact = user.contacts[id];
    res.render("contacts/show", { id, contact });
  };
  create = (req, res) => {
    const { contact } = req.body;
    const { user } = req.session;

    user.contacts.push(contact);
    res.redirect("/contatos");
  };
  edit = (req, res) => {
    const { id } = req.params;
    const { user } = req.session;
    const contact = user.contacts[id];
    res.render("contacts/edit", { id, contact, user });
  };
  update = (req, res) => {
    const { contact } = req.body;
    const { user } = req.session;
    user.contacts[req.params.id] = contact;
    res.redirect("/contatos");
  };
  destroy = (req, res) => {
    const { id } = req.params;
    const { user } = req.session;
    user.contacts.splice(id, 1);
    res.redirect("/contatos");
  };
}

module.exports = (app) => {
  return new ContactsContoller(app);
};
