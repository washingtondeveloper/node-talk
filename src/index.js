const express = require("express");
const expressSession = require("express-session");
const cookieParser = require("cookie-parser");
const methodOverride = require("method-override");
const path = require("path");
const consign = require("consign");
const expressLayouts = require("express-ejs-layouts");

const error = require("./middlewares/error");
const app = express();

/* Views */
app.set("view engine", "ejs");
app.set("views", path.join(__dirname, "views"));
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(methodOverride("_method"));
app.use(expressLayouts);
app.use(cookieParser("ntalk"));
app.use(expressSession());

app.use(express.static(path.join(__dirname, "..", "public")));

/* Inject */
consign({ cwd: "src" })
  .include("models")
  .then("controllers")
  .then("routes")
  .into(app);

// middleware de tratamento erros
app.use(error.notFound);
app.use(error.serverError);

module.exports = app;
